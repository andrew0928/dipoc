﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp6.Core
{
    public class Program
    {
        static void Main(string[] args)
        {
            var host = new HostBuilder()
                .ConfigureHostConfiguration((config) =>
                {
                    config.AddInMemoryCollection(new KeyValuePair<string, string>[] {
                        new KeyValuePair<string, string>("a", "A"),
                        new KeyValuePair<string, string>("b", "B"),
                        new KeyValuePair<string, string>("shop100/connectstr", "tcp://127.0.0.1:5672/shop100"),
                        new KeyValuePair<string, string>("shop101/connectstr", "tcp://127.0.0.1:5672/shop101"),
                        new KeyValuePair<string, string>("shop102/connectstr", "tcp://127.0.0.1:5672/shop102"),
                        new KeyValuePair<string, string>("shop103/connectstr", "tcp://127.0.0.1:5672/shop103"),
                        new KeyValuePair<string, string>("shop104/connectstr", "tcp://127.0.0.1:5672/shop104"),
                        new KeyValuePair<string, string>("shop105/connectstr", "tcp://127.0.0.1:5672/shop105"),
                    });
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<DemoService>();

                    services.AddScoped<CartContext>();
                    services.AddScoped<ConfigHelper>();
                })
                .Build();

            //for(int index = 0; index < 100; index++)
            //{
            //    using (var scope = host.Services.CreateScope())
            //    {
            //        var cart = scope.ServiceProvider.GetService<CartContext>();
            //        cart.ShopId = 100 + index;
            //        cart.UserId = $"member{index}@shop{cart.ShopId}.com";



            //        var chelper = scope.ServiceProvider.GetService<ConfigHelper>();
            //        Console.WriteLine(chelper["connectstr"]);
            //    }
            //}




            using (host)
            {
                Console.WriteLine("# starting...");
                host.StartAsync();
                Console.WriteLine("# started");

                Console.WriteLine("# wait for shutdown...");
                host.WaitForShutdown();

                Console.WriteLine("# host was shutted down...");
            }
        }
    }





    //public class DemoConfigProvider<T> : ConfigurationProvider
    //{
    //    public override void Load()
    //    {
    //        base.Load();
    //    }
    //    public override bool TryGet(string key, out string value)
    //    {
    //        value = $"value-of:{null}.{key}";
    //        return (key == "543");
    //    }
    //}

    //public class DemoConfigSource<T> : IConfigurationSource
    //{
    //    public DemoConfigSource()
    //    {
    //    }
            
    //    public IConfigurationProvider Build(IConfigurationBuilder builder)
    //    {
    //        return new DemoConfigProvider<T>();
    //    }
    //}






}
