﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp6.Core
{
    public class ConfigHelper
    {
        private CartContext _cart;
        private IConfiguration _config;


        public ConfigHelper(CartContext cart, IConfiguration config)
        {
            this._cart = cart;
            this._config = config;
        }

        public string this[string key] {
            get
            {
                return this._config[$"shop{_cart.ShopId}/{key}"];
            }
        }
    }
}
