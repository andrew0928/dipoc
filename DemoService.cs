﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp6.Core
{
    public class DemoService : BackgroundService
    {
        private IServiceScopeFactory _scopeFactory = null;
        public DemoService(IServiceScopeFactory scopeFactory)
        {
            this._scopeFactory = scopeFactory;
        }



        public override void Dispose()
        {
            Console.WriteLine("# disposed...");
            base.Dispose();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {

            for (int index = 0; index < int.MaxValue && stoppingToken.IsCancellationRequested == false; index++)
            {
                using (var scope = this._scopeFactory.CreateScope())
                {
                    {
                        var cart = scope.ServiceProvider.GetService<CartContext>();
                        cart.ShopId = 100 + index;
                        cart.UserId = $"member{index}@shop{cart.ShopId}.com";
                    }

                    var chelper = scope.ServiceProvider.GetService<ConfigHelper>();
                    var shop = scope.ServiceProvider.GetService<CartContext>();
                    Console.WriteLine("- current user: {0}@{1}.com, connect to database: {2}", shop.UserId, shop.ShopId, chelper["connectstr"]);
                    await Task.Delay(1000);
                }
            }
            await Task.Delay(1000);
            Console.WriteLine(".....");
            //return Task.CompletedTask;
        }
    }
}
